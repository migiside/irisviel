﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

public static class PropertyChangedEventHandlerExtensions
{
    public static void Raise<TResult>(this PropertyChangedEventHandler source, Expression<Func<TResult>> propertyName)
    {
        if (source == null) return;

        if (propertyName == null) throw new ArgumentNullException();

        var memberEx = propertyName.Body as MemberExpression;
        if (memberEx == null) throw new ArgumentException();

        var senderExpression = memberEx.Expression as ConstantExpression;
        if (senderExpression == null) throw new ArgumentException();

        var sender = senderExpression.Value;

        source(sender, new PropertyChangedEventArgs(memberEx.Member.Name));
    }
}
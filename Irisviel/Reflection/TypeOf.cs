﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Irisviel.Reflection
{
    public static class TypeOf<T>
    {
        public static string PropertyName<TProp>(Expression<Func<T, TProp>> expression)
        {
            var body = expression.Body as MemberExpression;
            if (body == null) throw new ArgumentException("'expression' should be a member expression");
            return body.Member.Name;
        }

        public static string MethodName(Expression<Action<T>> expression)
        {
            var body = expression.Body as MethodCallExpression;
            if (body == null) throw new ArgumentException("'expression' should be a method call expression");
            return body.Method.Name;
        }

        public static string MethodName<TMethod>(Expression<Func<T, TMethod>> expression)
        {
            var body = expression.Body as MethodCallExpression;
            if (body == null) throw new ArgumentException("'expression' should be a method call expression");
            return body.Method.Name;
        }
    }
}

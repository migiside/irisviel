﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Irisviel
{
    /// <summary>
    /// ObjectのライフサイクルをUsing文を用いることにより明示しやすくします
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LifeCycleObject<T> : IDisposable
    {
        public T Item { get; private set; }

        private readonly Action<T> _onDispose;

        public LifeCycleObject(T obj)
            : this(obj, _ => { })
        {
        }

        ~LifeCycleObject()
        {
            Dispose(false);
        }

        public LifeCycleObject(T obj, Action<T> onDispose)
        {
            Item = obj;
            _onDispose = onDispose;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    try
                    {
                        _onDispose(Item);
                    }
                    catch
                    {
                    }
                }
                _disposed = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Irisviel.IO
{
    /// <summary>
    /// 一時ディレクトリの位置情報、一時ファイルの作成を行います
    /// プログラムの終了時に、一時ディレクトリの削除を試みます
    /// 万が一削除されない場合でも、次回起動時に一時ディレクトリの削除を試みます
    /// 削除対象となるディレクトリは、このクラスで作成されたもののみです
    /// </summary>
    public sealed class TemporaryDirectory : IDisposable
    {
        private readonly TemporaryDirectory _parentTemporaryDirectory;

        public string Name { get { return Path.GetDirectoryName(FullName); } }

        public string FullName { get; private set; }

        public string BasePath { get; private set; }

        internal TemporaryDirectory(string path)
        {
            FullName = CreateUniqueTemporaryDirectory(BasePath = path);
        }

        internal TemporaryDirectory(TemporaryDirectory directory)
        {
            _parentTemporaryDirectory = directory;
            FullName = CreateUniqueTemporaryDirectory(BasePath = directory.FullName);
        }

        ~TemporaryDirectory()
        {
            try
            {
                Directory.Delete(FullName);
            }
            catch
            {
            }
        }

        private string CreateUniqueTemporaryDirectory(string path)
        {
            return Observable.Defer(() => Observable.Start(() => Directory.CreateDirectory(Path.Combine(path, Temporary.ComputeDirectoryName(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)))).FullName)).Retry().FirstAsync().Wait();
        }

        /// <summary>
        /// 現在のディレクトリに新しい一時フォルダを作成します
        /// </summary>
        /// <returns></returns>
        public TemporaryDirectory CreateTemporaryDirectory()
        {
            return new TemporaryDirectory(this);
        }

        /// <summary>
        /// 新しい一時ファイルを作成します
        /// </summary>
        /// <returns></returns>
        public TemporaryFile CreateTemporaryFile()
        {
            using (var fs = Observable.Defer(() => Observable.Start(() => File.Create(Path.Combine(FullName, DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture))))).Retry().FirstAsync().Wait())
            {
                return new TemporaryFile(this, fs.Name);
            }
        }

        /// <summary>
        /// このオブジェクトのライフサイクルを明示的に示すためのDisposeです
        /// Using文と一緒に使用することにより、このオブジェクトのライフサイクルを明示することが出来ます
        /// Disposeの呼び出しでこのディレクトリが削除されることはありません
        /// このオブジェクトが回収されたときにディレクトリが削除されます
        /// </summary>
        public void Dispose()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Irisviel.IO
{
    public class TemporaryFile : IDisposable
    {
        private readonly TemporaryDirectory _parentDirectory;

        public string Name { get; private set; }

        public string FullName
        {
            get { return Path.Combine(_parentDirectory.FullName, Name); }
        }

        ~TemporaryFile()
        {
            Dispose(false);
        }

        internal TemporaryFile(TemporaryDirectory directory, string name)
        {
            _parentDirectory = directory;
            Name = name;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed; // indicates if Dispose has been called

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    try
                    {
                        File.Delete(FullName);
                    }
                    catch
                    {
                    }
                }
                _disposed = true;
            }
        }
    }

}

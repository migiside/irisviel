﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;

namespace Irisviel.IO
{
    /// <summary>
    /// 一時ファイル、一時ディレクトリを扱います
    /// </summary>
    public static class Temporary
    {
        private static readonly ConcurrentDictionary<string, TemporaryDirectory> temporaryInfos = new ConcurrentDictionary<string, TemporaryDirectory>();

        public static List<TemporaryDirectory> RootTemporaryDirectories { get { return temporaryInfos.Values.ToList(); } }

        /// <summary>
        /// 規定のディレクトリに一時ディレクトリを作成します
        /// </summary>
        /// <returns></returns>
        public static TemporaryDirectory GetUniqueTemporaryDirectory()
        {
            return GetUniqueTemporaryDirectory(Path.GetTempPath());
        }

        /// <summary>
        /// 指定された場所に一時ディレクトリを作成します
        /// </summary>
        /// <param name="basePath"></param>
        /// <returns></returns>
        public static TemporaryDirectory GetUniqueTemporaryDirectory(string basePath)
        {
            return temporaryInfos.GetOrAdd(basePath, _ =>
                                                         {
                                                             Cleanup(_);
                                                             return new TemporaryDirectory(_);
                                                         });
        }

        internal static string ComputeDirectoryName(string name)
        {
            return
               name + "_" + SHA256.Create().ComputeHash(Encoding.Unicode.GetBytes(name + "Irisviel.IO.Temporary")).Select(b => b.ToString("x2")).
                    Aggregate((a, b) => a + b);
        }

        private static IEnumerable<string> GetUniqueTemporaryDirectories(string path)
        {
            return Directory.GetDirectories(path)
                .Select(_ => Regex.Match(_, "(?<ticks>[0-9]+)_(?<hash>[0-9a-f]+)"))
                .Where(_ => _.Success)
                .Select(_ => new { Ticks = _.Groups["ticks"].Value, Hash = _.Groups["hash"].Value, Name = _.Value })
                .Where(_ => ComputeDirectoryName(_.Ticks) == _.Name)
                .Select(_ => _.Name);
        }

        private static void Cleanup(string path)
        {
            foreach (var dir in GetUniqueTemporaryDirectories(path))
            {
                try
                {
                    Directory.Delete(Path.Combine(path, dir), true);
                }
                catch
                {
                }
            }
        }
    }
}

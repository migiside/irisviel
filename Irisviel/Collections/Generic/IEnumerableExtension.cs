﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class IEnumerableExtension
{
    /// <summary>
    /// コレクションを指定された個数を格納するサブリストに分割します
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="chunkSize"></param>
    /// <returns></returns>
    public static IEnumerable<IList<T>> ToSublists<T>(this IEnumerable<T> source, int chunkSize)
    {
        var current = new List<T>();

        foreach (var s in source)
        {
            current.Add(s);

            if (current.Count == chunkSize)
            {
                yield return current;
                current = new List<T>();
            }
        }
        if (current.Count > 0)
        {
            yield return current;
        }
        yield break;
    }
}
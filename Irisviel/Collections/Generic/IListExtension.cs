﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class IListExtension
{
    public static int LeftEdgeSearch<T>(this IList<T> source, T value)
    {
        var index = -1;
        var edge = source.Count;
        var array = source.ToArray();

        while ((edge = Array.BinarySearch<T>(array, 0, edge, value)) >= 0)
        {
            index = edge;
        }
        return index;
    }

    public static int RightEdgeSearch<T>(this IList<T> source, T value)
    {
        var index = -1;
        var edge = -1;
        var length = source.Count;
        var array = source.ToArray();

        while ((edge = Array.BinarySearch<T>(array, edge + 1, length, value)) >= 0)
        {
            index = edge;
            length = source.Count - (edge + 1);
        }
        return index;
    }
}


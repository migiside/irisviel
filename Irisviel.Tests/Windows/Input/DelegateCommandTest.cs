﻿using Irisviel.Windows.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Irisviel.Tests.Windows.Input
{
    public class DelegateCommandTest
    {
        [Fact]
        public void EnableTest()
        {
            var com = new DelegateCommand(_ => { }, _ => true);
            Assert.True(com.CanExecute(null));
        }

        [Fact]
        public void DisableTest()
        {
            var com = new DelegateCommand(_ => { }, _ => false);
            Assert.False(com.CanExecute(null));
        }

        [Fact]
        public void FuncCallTest()
        {
            var com = new DelegateCommand(_ => Assert.True(true), _ => true);
            com.Execute(null);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irisviel.ComponentModel;
using Xunit;

namespace Irisviel.Tests.ComponentModel
{
    public class NotifiableModelBaseTest
    {
        class TestClass : NotifiableModelBase
        {
            private int _testProperty;
            public int TestProperty
            {
                get { return _testProperty; }
                set
                {
                    _testProperty = value;
                    OnPropertyChanged();
                }
            }
        }

        [Fact]
        public void NotifyTest()
        {
            var inst = new TestClass();
            inst.PropertyChanged += (s, e) => { Assert.Equal(e.PropertyName, "TestProperty"); };
            inst.TestProperty = 1;
        }
    }
}

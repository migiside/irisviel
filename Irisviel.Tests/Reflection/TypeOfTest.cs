using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq.Expressions;
using Irisviel.Reflection;
using Xunit;

namespace Irisviel.Tests.Reflection
{
    public partial class TypeOfTest
    {
        [Fact]
        public void PropertyName1()
        {
            Assert.Equal(TypeOf<Sample>.PropertyName(x => x.X), "X");
        }

        [Fact]
        public void PropertyName2()
        {
            Assert.NotEqual(TypeOf<Sample>.PropertyName(x => x.X), "Y");
        }

        [Fact]
        public void PropertyName3()
        {
            Assert.Throws(typeof(ArgumentException), () => TypeOf<Sample>.PropertyName(x => x.ToString()));
        }

        [Fact]
        public void MethodName1()
        {
            Assert.Equal(TypeOf<Sample>.MethodName(s => s.Method1()), "Method1");
        }

        [Fact]
        public void MethodName2()
        {
            Assert.Equal(TypeOf<Sample>.MethodName(s => s.Method2()), "Method2");
        }

        [Fact]
        public void MethodName3()
        {
            Assert.Equal(TypeOf<Sample>.MethodName(s => s.Method3(s)), "Method3");
        }

        [Fact]
        public void MethodName4()
        {
            Assert.Throws(typeof(ArgumentException), () => TypeOf<Sample>.MethodName(s => s.X));
        }
    }

    class Sample
    {
        public string X { get; set; }

        public void Method1() { }

        public string Method2()
        {
            return String.Empty;
        }

        public Sample Method3(Sample sample)
        {
            return sample;
        }
    }
}

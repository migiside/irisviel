﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Irisviel.IO;
using Xunit;

namespace Irisviel.Tests.IO
{
    public class TemporaryDirectoryTest
    {
        [Fact]
        public void TemporaryDirectory1()
        {
            string dirName = "IrisvielTestDirecotry_TemporaryInfo1" + DateTime.Now.Ticks.ToString();
            var path = Path.Combine(Path.Combine(Path.GetTempPath(), dirName), "0_cd3133986db32c2c2873ded1ab6a3649a8b500af2d3899aa46af2d6f468290de");
            Directory.CreateDirectory(path);
            Temporary.GetUniqueTemporaryDirectory(Path.Combine(Path.GetTempPath(), dirName));
            Assert.False(Directory.Exists(path));
        }

        [Fact]
        public void CreateTemporaryFile1()
        {
            string filePath;
            using (var file = Temporary.GetUniqueTemporaryDirectory().CreateTemporaryFile())
            {
                filePath = file.FullName;
                Assert.True(File.Exists(file.FullName));
            }
            Assert.False(File.Exists(filePath));
        }

        [Fact]
        public void CreateSubTemporaryDirectory()
        {
            using (var dir = Temporary.GetUniqueTemporaryDirectory().CreateTemporaryDirectory())
            {
                Assert.True(Directory.Exists(dir.FullName));
            }
        }
    }
}

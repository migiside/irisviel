﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Irisviel.IO;
using Xunit;

namespace Irisviel.Tests.IO
{
    public class TemporaryTest
    {
        [Fact]
        public void GetUniqueTemporaryDirectory1()
        {
            Assert.True(Directory.Exists(Temporary.GetUniqueTemporaryDirectory().FullName));
        }

        /// <summary>
        /// 二度呼び出しても同じ場所が返るか？
        /// </summary>
        [Fact]
        public void GetUniqueTemporaryDirectory2()
        {
            Assert.Equal(Temporary.GetUniqueTemporaryDirectory().FullName, Temporary.GetUniqueTemporaryDirectory().FullName);
        }
    }
}

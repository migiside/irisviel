﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Irisviel.Tests.Collections.Generic
{
    public partial class IEnumerableExtensionTest
    {
        [Fact]
        public void ToSublists1()
        {
            var list = Enumerable.Range(0, 9);
            Assert.Equal(3, list.ToSublists(3).Count());
        }

        [Fact]
        public void ToSublists2()
        {
            var list = Enumerable.Range(0, 10);
            Assert.Equal(4, list.ToSublists(3).Count());
        }

        [Fact]
        public void ToSublists3()
        {
            var list = new List<int>();
            Assert.Empty(list.ToSublists(10));
        }
    }
}

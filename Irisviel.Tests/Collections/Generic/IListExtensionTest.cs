﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Irisviel.Tests.Collections.Generic
{
    public partial class IListExtensionTest
    {
        [Fact]
        public void LeftEdgeSearch1()
        {
            var list = ListGenerator(10);
            //存在する値でのテスト
            for (int i = 1; i <= 10; i++)
            {
                var value = i;
                var expected = i * (i - 1) / 2;
                var actual = list.LeftEdgeSearch(value);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void LeftEdgeSearch2()
        {
            var list = ListGenerator(10);
            //存在しない値でのテスト
            var actual = list.LeftEdgeSearch(0);
            const int expected = -1;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RightEdgeSearch1()
        {
            var list = ListGenerator(10);
            //存在する値でのテスト
            for (int i = 1; i <= 10; i++)
            {
                var value = i;
                var expected = (i - 1) * (i + 2) / 2;
                var actual = list.RightEdgeSearch(value);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void RightEdgeSearch2()
        {
            var list = ListGenerator(10);
            //存在しない値でのテスト
            var actual = list.RightEdgeSearch(0);
            const int expected = -1;
            Assert.Equal(expected, actual);
        }

        private List<int> ListGenerator(int num)
        {
            var list = new List<int>();
            for (var i = 0; i <= num; i++)
            {
                for (var j = 0; j < i; j++)
                {
                    list.Add(i);
                }
            }

            return list;
        }
    }
}

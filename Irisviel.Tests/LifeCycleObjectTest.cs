﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Irisviel.Tests
{

    public class LifeCycleObjectTest
    {
        [Fact]
        public void EnsureOnDisposeActionCallTest()
        {
            using (var obj = new LifeCycleObject<string>(String.Empty, _ => Assert.Equal(String.Empty, _)))
            {
            }
        }
    }
}
